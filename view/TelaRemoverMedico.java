package view;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class TelaRemoverMedico extends JInternalFrame {
	private JTextField textNome;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaRemoverMedico frame = new TelaRemoverMedico();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaRemoverMedico() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(12, 12, 66, 15);
		getContentPane().add(lblNome);
		
		textNome = new JTextField();
		textNome.setBounds(67, 10, 361, 19);
		getContentPane().add(textNome);
		textNome.setColumns(10);
		
		JButton btnSubmeter = new JButton("Submeter");
		btnSubmeter.setBounds(318, 231, 110, 25);
		getContentPane().add(btnSubmeter);

	}

}
