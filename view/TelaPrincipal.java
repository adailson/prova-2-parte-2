package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JDesktopPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TelaPrincipal extends JFrame {

	private JPanel contentPane;
	private TelaCadastroMedico cadastroMedico;
	private TelaCadastroPaciente cadastroPaciente;
	private TelaCadastroConsulta cadastroConsulta;
	private TelaBuscarMedico buscaMedico;
	private TelaBuscarPaciente buscaPaciente;
	private TelaBuscaConsulta buscaConsulta;
	private TelaRemoverMedico removeMedico;
	private TelaRemoverPaciente removePaciente;
	private TelaRemoverConsulta removeConsulta;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPrincipal frame = new TelaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 671, 523);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnMenu = new JMenu("Menu");
		menuBar.add(mnMenu);
		
		JMenu mnCadastro = new JMenu("Cadastro");
		mnMenu.add(mnCadastro);
		
		JMenuItem mntmMedico = new JMenuItem("Medico");
		
		mnCadastro.add(mntmMedico);
		
		JMenuItem mntmPaciente = new JMenuItem("Paciente");
		
		mnCadastro.add(mntmPaciente);
		
		JMenuItem mntmConsulta = new JMenuItem("Consulta");
		
		mnCadastro.add(mntmConsulta);
		
		JMenu mnBuscar = new JMenu("Buscar");
		mnMenu.add(mnBuscar);
		
		JMenuItem mntmMedico_1 = new JMenuItem("Medico");
		
		mnBuscar.add(mntmMedico_1);
		
		JMenuItem mntmPaciente_1 = new JMenuItem("Paciente");
		
		mnBuscar.add(mntmPaciente_1);
		
		JMenuItem mntmConsulta_1 = new JMenuItem("Consulta");
		
		mnBuscar.add(mntmConsulta_1);
		
		JMenu mnRemover = new JMenu("Remover");
		mnMenu.add(mnRemover);
		
		JMenuItem mntmMdico = new JMenuItem("Médico");
		
		mnRemover.add(mntmMdico);
		
		JMenuItem mntmPaciente_2 = new JMenuItem("Paciente");
		
		mnRemover.add(mntmPaciente_2);
		
		JMenuItem mntmConsulta_2 = new JMenuItem("Consulta");
		
		mnRemover.add(mntmConsulta_2);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		final JDesktopPane desktopPane = new JDesktopPane();
		contentPane.add(desktopPane, BorderLayout.CENTER);
		
		mntmMedico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(cadastroMedico == null || cadastroMedico.isClosed()){
					cadastroMedico = new TelaCadastroMedico();
					desktopPane.add(cadastroMedico);
					cadastroMedico.show();
				}
			}
		});
		mntmPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(cadastroPaciente == null || cadastroPaciente.isClosed()){
					cadastroPaciente = new TelaCadastroPaciente();
					desktopPane.add(cadastroPaciente);
					cadastroPaciente.show();
				}
			}
		});
		mntmConsulta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(cadastroConsulta == null || cadastroConsulta.isClosed()){
					cadastroConsulta = new TelaCadastroConsulta();
					desktopPane.add(cadastroConsulta);
					cadastroConsulta.show();
				}
			}
		});
		mntmMedico_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(buscaMedico == null || buscaMedico.isClosed()){
					buscaMedico = new TelaBuscarMedico();
					desktopPane.add(buscaMedico);
					buscaMedico.show();
				}
			}
		});
		mntmPaciente_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(buscaPaciente == null || buscaPaciente.isClosed()){
					buscaPaciente = new TelaBuscarPaciente();
					desktopPane.add(buscaPaciente);
					buscaPaciente.show();
				}
			}
		});
		mntmConsulta_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(buscaConsulta == null || buscaConsulta.isClosed()){
					buscaConsulta = new TelaBuscaConsulta();
					desktopPane.add(buscaConsulta);
					buscaConsulta.show();
				}
			}
		});
		mntmMdico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(removeMedico == null || removeMedico.isClosed()){
					removeMedico = new TelaRemoverMedico();
					desktopPane.add(removeMedico);
					removeMedico.show();
				}
			}
		});
		mntmPaciente_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(removePaciente == null || removePaciente.isClosed()){
					removePaciente = new TelaRemoverPaciente();
					desktopPane.add(removePaciente);
					removePaciente.show();
				}
			}
		});
		mntmConsulta_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(removeConsulta == null || removeConsulta.isClosed()){
					removeConsulta = new TelaRemoverConsulta();
					desktopPane.add(removeConsulta);
					removeConsulta.show();
				}
			}
		});
	}

}
