package view;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class TelaCadastroPaciente extends JInternalFrame {
	private JTextField textNome;
	private JTextField textTelefone;
	private JTextField textSexo;
	private JTextField textCpf;
	private JTextField textQueixa;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCadastroPaciente frame = new TelaCadastroPaciente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaCadastroPaciente() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(12, 12, 66, 15);
		getContentPane().add(lblNome);
		
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setBounds(12, 39, 66, 15);
		getContentPane().add(lblTelefone);
		
		JLabel lblSexo = new JLabel("Sexo");
		lblSexo.setBounds(12, 68, 66, 15);
		getContentPane().add(lblSexo);
		
		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setBounds(12, 95, 66, 15);
		getContentPane().add(lblCpf);
		
		JLabel lblQueixa = new JLabel("Queixa");
		lblQueixa.setBounds(12, 122, 66, 15);
		getContentPane().add(lblQueixa);
		
		textNome = new JTextField();
		textNome.setBounds(58, 10, 370, 19);
		getContentPane().add(textNome);
		textNome.setColumns(10);
		
		textTelefone = new JTextField();
		textTelefone.setBounds(81, 37, 347, 19);
		getContentPane().add(textTelefone);
		textTelefone.setColumns(10);
		
		textSexo = new JTextField();
		textSexo.setBounds(58, 66, 370, 19);
		getContentPane().add(textSexo);
		textSexo.setColumns(10);
		
		textCpf = new JTextField();
		textCpf.setBounds(54, 95, 374, 19);
		getContentPane().add(textCpf);
		textCpf.setColumns(10);
		
		textQueixa = new JTextField();
		textQueixa.setBounds(64, 120, 364, 19);
		getContentPane().add(textQueixa);
		textQueixa.setColumns(10);

	}

}
