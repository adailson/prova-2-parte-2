package view;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class TelaBuscarPaciente extends JInternalFrame {
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaBuscarPaciente frame = new TelaBuscarPaciente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaBuscarPaciente() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(12, 12, 73, 23);
		getContentPane().add(lblNome);
		
		textField = new JTextField();
		textField.setBounds(58, 14, 370, 19);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnSubmeter = new JButton("Submeter");
		btnSubmeter.setBounds(318, 231, 110, 25);
		getContentPane().add(btnSubmeter);

	}

}
