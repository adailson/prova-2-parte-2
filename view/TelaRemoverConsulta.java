package view;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;

public class TelaRemoverConsulta extends JInternalFrame {
	private JTextField textNome;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaRemoverConsulta frame = new TelaRemoverConsulta();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaRemoverConsulta() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Horário");
		lblNewLabel.setBounds(12, 12, 66, 15);
		getContentPane().add(lblNewLabel);
		
		textNome = new JTextField();
		textNome.setBounds(71, 10, 357, 19);
		getContentPane().add(textNome);
		textNome.setColumns(10);
		
		JToggleButton tglbtnSubmeter = new JToggleButton("Submeter");
		tglbtnSubmeter.setBounds(310, 231, 118, 25);
		getContentPane().add(tglbtnSubmeter);

	}
}
