package view;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.BoxLayout;
import javax.swing.JLabel;

import javax.swing.JTextField;
public class TelaCadastroMedico extends JInternalFrame {
	private JTextField textNome;
	private JTextField textEspecializacao;
	private JTextField textConvenio;
	private JTextField textTelefone;
	private JTextField textSexo;
	private JTextField textCpf;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCadastroMedico frame = new TelaCadastroMedico();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaCadastroMedico() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(12, 12, 66, 15);
		getContentPane().add(lblNome);
		
		textNome = new JTextField();
		textNome.setBounds(65, 10, 339, 19);
		getContentPane().add(textNome);
		textNome.setColumns(10);
		
		JLabel lblEspecializacao = new JLabel("Especializacao");
		lblEspecializacao.setBounds(12, 119, 98, 15);
		getContentPane().add(lblEspecializacao);
		
		textEspecializacao = new JTextField();
		textEspecializacao.setBounds(118, 117, 286, 19);
		getContentPane().add(textEspecializacao);
		textEspecializacao.setColumns(10);
		
		textConvenio = new JTextField();
		textConvenio.setBounds(80, 148, 327, 19);
		getContentPane().add(textConvenio);
		textConvenio.setColumns(10);
		
		JLabel lblConvnio = new JLabel("Convênio");
		lblConvnio.setBounds(12, 150, 66, 15);
		getContentPane().add(lblConvnio);
		
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setBounds(12, 36, 66, 15);
		getContentPane().add(lblTelefone);
		
		textTelefone = new JTextField();
		textTelefone.setBounds(80, 34, 324, 19);
		getContentPane().add(textTelefone);
		textTelefone.setColumns(10);
		
		JLabel lblSexo = new JLabel("Sexo");
		lblSexo.setBounds(12, 63, 66, 15);
		getContentPane().add(lblSexo);
		
		textSexo = new JTextField();
		textSexo.setBounds(48, 61, 356, 19);
		getContentPane().add(textSexo);
		textSexo.setColumns(10);
		
		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setBounds(12, 92, 66, 15);
		getContentPane().add(lblCpf);
		
		textCpf = new JTextField();
		textCpf.setBounds(48, 88, 356, 19);
		getContentPane().add(textCpf);
		textCpf.setColumns(10);

	}
}
