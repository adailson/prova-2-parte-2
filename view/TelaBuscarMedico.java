package view;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.JToggleButton;

public class TelaBuscarMedico extends JInternalFrame {
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaBuscarMedico frame = new TelaBuscarMedico();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaBuscarMedico() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(12, 12, 66, 15);
		getContentPane().add(lblNome);
		
		textField = new JTextField();
		textField.setBounds(55, 10, 373, 19);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JToggleButton tglbtnSubmeter = new JToggleButton("Submeter");
		tglbtnSubmeter.setBounds(317, 231, 111, 25);
		getContentPane().add(tglbtnSubmeter);

	}
}
