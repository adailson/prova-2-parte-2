package view;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class TelaCadastroConsulta extends JInternalFrame {
	private JTextField textField;
	private JTextField textDiagnostico;
	private JTextField textMedico;
	private JTextField textPaciente;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCadastroConsulta frame = new TelaCadastroConsulta();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaCadastroConsulta() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblHorario = new JLabel("Horário");
		lblHorario.setBounds(12, 12, 66, 15);
		getContentPane().add(lblHorario);
		
		textField = new JTextField();
		textField.setBounds(72, 10, 356, 19);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblDiagnstico = new JLabel("Diagnóstico");
		lblDiagnstico.setBounds(12, 39, 86, 15);
		getContentPane().add(lblDiagnstico);
		
		textDiagnostico = new JTextField();
		textDiagnostico.setBounds(98, 37, 330, 19);
		getContentPane().add(textDiagnostico);
		textDiagnostico.setColumns(10);
		
		JLabel lblMdico = new JLabel("Médico");
		lblMdico.setBounds(12, 66, 66, 15);
		getContentPane().add(lblMdico);
		
		textMedico = new JTextField();
		textMedico.setBounds(72, 64, 356, 19);
		getContentPane().add(textMedico);
		textMedico.setColumns(10);
		
		JLabel lblPaciente = new JLabel("Paciente");
		lblPaciente.setBounds(12, 93, 66, 15);
		getContentPane().add(lblPaciente);
		
		textPaciente = new JTextField();
		textPaciente.setBounds(82, 91, 346, 19);
		getContentPane().add(textPaciente);
		textPaciente.setColumns(10);

	}

}
